# Исходные данные #


```
#!sql

CREATE TABLE `contact` (
	`contact_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ',
	`contact_name` VARCHAR(50) NOT NULL COMMENT 'Имя',
	PRIMARY KEY (`contact_id`),
	INDEX `contact_name` (`contact_name`)
)
COMMENT='Контакты'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=11
;
```



```
#!sql

CREATE TABLE `contact_frendship` (
	`contact_frendship_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ',
	`contact_frendship_subject_id` INT(11) NOT NULL COMMENT 'Субъект дружбы',
	`contact_frendship_object_id` INT(11) NOT NULL COMMENT 'Объект дружбы',
	PRIMARY KEY (`contact_frendship_id`),
	INDEX `contact_frendship_subject_id` (`contact_frendship_subject_id`)
)
COMMENT='Кто с кем дружит'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=15
;
```